.. Placa de Panama documentation master file, created by
   sphinx-quickstart on Sun Jul 21 02:38:10 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Bienvenido a la documentación Placa de Panama
=============================================
.. image:: _static/att.jpg
    :align: center
    :alt: Placa de Panamá


*Placa de Panamá* es una funcionalidad que ayuda a validar si el
numero introducido por la persona es un número valido de placa.

La placas están compuesta de:

- 6 digitos
- Siglas que definen el tipo de placa.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

.. automodule:: placa
    :members:

Indices y tablas
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
