import placa
"""
Se importa un modulo llamado placa desde otro archivo .py.
"""

name = "placanr_pkg"
numero = placa.intro()
objeto = placa.LicensePlate(numero)
objeto.verify()
