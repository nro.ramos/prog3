import re


def rev_placa(placa):
    placam = placa.upper()

    if re.match('CP[0-9][0-9][0-9][0-9]',placam ):
        print("\nPlaca: ",placam ," Tipo: Oficial-ACP")
    else:
        if re.match('CC|CH|CD|MI|PH|PE[0-9][0-9][0-9][0-9]',placam ):
            print("\nPlaca: ",placam ," Tipo: Diplomatica-Relaciones Exteriores")
        else:
            if re.match('ADM|MMI|MCD|RCD|RMI|[0-9][0-9][0-9]',placam ):
                print("\nPlaca: ", placam, " Tipo: Diplomatica-Relaciones Exteriores")
            else:
                if re.match('MADM[0-9][0-9]', placam):
                    print("\nPlaca: ", placam, " Tipo: Diplomatica-Relaciones Exteriores")
                else:
                    if re.match('PR[0-9][0-9][0-9][0-9]', placam):
                        print("\nPlaca: ", placam, " Tipo: Servicios Especiales-Prensa")
                    else:
                        if re.match('HP[0-9][0-9][0-9][0-9]', placam):
                            print("\nPlaca: ", placam, " Tipo: Servicios Especiales-Radioaficionado")
                        else:
                            if re.match('D[0-9][0-9][0-9][0-9][0-9]', placam):
                                print("\nPlaca: ", placam, " Tipo: Otro-Demostracion")
                            else:
                                if re.match('E[0-9][0-9][0-9][0-9][0-9]', placam):
                                    print("\nPlaca: ", placam, " Tipo: Otro-Especial")
                                else:
                                    if re.match('M[0-9][0-9][0-9][0-9][0-9]', placam):
                                        print("\nPlaca: ", placam, " Tipo: Particular-Motocicleta")
                                    else:
                                        if re.match('[0-9][0-9][0-9][0-9][0-9][0-9]',placam ):
                                            print("\nPlaca: ",placam ," Tipo: Particular")
                                        else:
                                            if re.match('[A-Z][A-Z][0-9][0-9][0-9][0-9]',placam ):
                                                print("\nPlaca: ",placam ," Tipo: Particular")


if __name__ == "__main__":
    print("SISTEMA DE PLACAS")
    print("-------------------\n")

    placa = input("Introduzca la placa a evaluar: ")
    tamano = len(placa)

    if tamano== 6:
        rev_placa(placa)
    else:
        print('\nLa placa no es valida!!!')