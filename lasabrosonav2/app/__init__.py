#coding=iso-8859-15
if __name__ == "__main__":
	print("FONDA LA SABROSONA")
	print("-------------------\n")

	cliente = input("Cliente: ")
	
	print("\nMENU")
	menu = ['1. Mondongo', '2. Lengua', '3. Pata']
	for i in menu:
		print(i)
	
	while True:
		try:
			opcion = int(input("\nOpcion: "))
		except:
			opcion = -1
		if opcion == 1:
			comida = ['mondongo', 1.50]
			break
		elif opcion == 2:
			comida = ['lengua', 0.75]
			break
		elif opcion == 3:
			comida = ['pata', 2.00]
			break
		else:
			print("Opcion invalida")
	total = round((comida[1] * 1.07),2)

	print("\n"+ cliente + ", debe pagar $" + str(total))
	
	pago = input("\nPago? (S/N)")
	if pago.upper()== "S":
		print("\nToma tu", comida[0] + ",",cliente)
	else:
		print('\nPedido cancelado!!!')
