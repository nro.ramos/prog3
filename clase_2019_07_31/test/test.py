import unittest
from app import sumar
from app import resta
from app import multiplicar

class SumaTest(unittest.TestCase):
    def test_sumar(self):
        self.assertEqual(sumar(2, 2), 4)

class RestaTest(unittest.TestCase):
    def test_resta(self):
        self.assertEqual(resta(6, 3), 3)

class MultiplicarTest(unittest.TestCase):
    def test_multiplicar(self):
        self.assertEqual(multiplicar(3, 3), 9)

if __name__ == '__main__':
    unittest.main()