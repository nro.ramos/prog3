#coding=iso-8859-15
def calcular_area(lado):
	area = lado * lado
	return area

def saludar():
	print("Bienvenido Sr. Cuadrado\n")

if __name__ == "__main__":
	saludar()
	lado = float(input("Lado: "))
	resultado = calcular_area(lado)
	print("Area =", resultado)
