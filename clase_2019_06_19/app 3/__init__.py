class Perro:
	especie = 'mamifero'

	def __init__(self, nombre, edad):
		self.nombre = nombre
		self.edad = edad

	def descripcion(self):
		return "{} tiene {} anos de edad".format(self.nombre, self.edad)

	def hablar(self, sonido):
		return "{} dice {}".format(self.nombre, sonido)

#Clase hija de la Clase Perro
class Rottweiler(Perro):
	def correr(self, velocidad):
		print("{} corre a {}".format(self.nombre, velocidad))

lassie = Perro("Lassie", 5)
print(lassie.descripcion())
print(lassie.hablar("Woof woof"))

roco = Rottweiler("Roco", 10)
roco.correr(50)

#La clase hija puede usar los metodos de la clase Padre
print(roco.descripcion())
print(roco.hablar("Que es lo que eh"))